const express = require('express');
const router = express.Router();
const UserController = require('../controllers/userController');
const authController = require('../controllers/authController');


router.get('/', UserController.getAllUsers);
router.post('/whitelist/:id', UserController.whitelist);
router.post('/sendmails', UserController.sendMails);
// router.get('/:id', UserController.getUser);
router.put('/:id', UserController.updateUser);
router.post('/filter', UserController.getAllUsers);
router.post('/getUserByWallet', UserController.getUserByWallet);
router.post('/', UserController.addUser);
router.delete('/:id', UserController.deleteUser);


// Protect all routes after this middleware
router.use(authController.protect);

router.post('/send', UserController.transfer);
router.post('/isauthorized', authController.isauthorized);


module.exports = router;