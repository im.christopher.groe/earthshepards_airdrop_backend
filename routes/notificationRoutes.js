const express = require('express');
const router = express.Router();
const NotificationController = require('../controllers/notificationController');

// Protect all routes after this middleware
// router.use(authController.protect);

router.get('/', NotificationController.getAllNotifications);
router.get('/:id', NotificationController.getNotification);
router.post('/', NotificationController.addNotification);
router.put('/:id', NotificationController.updateNotification);
router.delete('/:id', NotificationController.deleteNotification);


module.exports = router;