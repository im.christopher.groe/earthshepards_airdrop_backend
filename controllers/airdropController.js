const base = require('./baseController');
const APIFeatures = require('../utils/apiFeatures');
const Airdrop = require('../models/airdropModel');

const { ApiPromise, WsProvider } = require('@polkadot/api');
const { Keyring } = require('@polkadot/keyring');

const wsProvider = new WsProvider("wss://www.shearnode.com");

exports.getAllAirdrops = base.getAll(Airdrop);
exports.getAirdrop = base.getOne(Airdrop);
exports.updateAirdrop = base.updateOne(Airdrop);
exports.deleteAirdrop = base.deleteOne(Airdrop);

exports.getRemainAmount = async(req, res, next) => {
    const counts = await Airdrop.count();

    res.status(200).json({
        status: 'success',
        amount: 10000 - counts
    });
}

exports.addAirdrop = async(req, res, next) => {
    try {
        const airdrops = await Airdrop.count();
        console.log(airdrops, req.body.wallet)

        if(airdrops >= 10000) {
            res.status(401).json({
                status: 'Failed',
                message: 'Airdrop has already finished'
            });
            return;
        }

        const wallet = await Airdrop.findOne({ wallet: req.body.wallet });
        console.log(wallet)
        if(wallet) {
            res.status(401).json({
                status: 'Failed',
                message: 'You have already received airdrop'
            });
            return;
        }

        const api = await ApiPromise.create({ provider: wsProvider });

        const keyring = new Keyring({ type: 'sr25519' });

        // Add Alice to our keyring with a hard-derivation path (empty phrase, so uses dev)
        const alice = keyring.addFromUri('//Alice');
        const { nonce } = await api.query.system.account(alice.address);

        // console.log('Sending', 4, 'from', alice.address, 'to', '5HYVeHiZAwUpftT5d1ThmZPLvEz4DFmUFuXfYvmCtSYpKUp8', 'with nonce', nonce.toString());

        const transfer = api.tx.balances.transfer(String(req.body.wallet), 4 * 1e12);

        // Sign and send the transaction using our account
        await transfer.signAndSend(alice, ({ events = [], status, txHash }) => {
            console.log(status.isFinalized ? `😉 Finalized. Block hash: ${status.asFinalized.toString()}` : `Current transaction status: ${status.type}`)

            events.forEach( async ({ _, event: { data, method, section } }) => {
                if ((section + ":" + method) === 'system:ExtrinsicFailed' ) {
                } else if (section + ":" + method === 'system:ExtrinsicSuccess' ) {
                    console.log(`❤️️ Transaction successful! tx hash: ${txHash}`)
                }
            });
        });
        
        await Airdrop.create(req.body);
                        
        res.status(200).json({
            status: 'success',
            message: 'Airdrop sent successfully.'
        });

    } catch (error) {
        next(error);
    }
};