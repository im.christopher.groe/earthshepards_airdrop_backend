const Reward = require('../models/rewardModel');
const base = require('./baseController');
const APIFeatures = require('../utils/apiFeatures');

const { ApiPromise, WsProvider } = require('@polkadot/api');
const { Keyring } = require('@polkadot/keyring');

const wsProvider = new WsProvider("wss://www.shearnode.com");

exports.getAllRewards = async(req, res, next) => {
    try {
        const doc = await Reward.find(req.body).populate('user_id').populate('product_id');
        
        res.status(200).json({
            status: 'success',
            results: doc.length,
            data: {
                data: doc
            }
        });
        
    } catch (error) {
        next(error);
    }
};
exports.getReward = base.getOne(Reward);

// Don't update password on this 
exports.updateReward = async(req, res, next) => {
    try {
        const doc = await Reward.findByIdAndUpdate(req.params.id, req.body, {
            new: true,
            runValidators: true
        });

        if (!doc) {
            return next(new AppError(404, 'fail', 'No document found with that id'), req, res, next);
        }

        if(req.body.status === 'Accepted') {
            const rewardInfo = await Reward.findOne({ _id: req.params.id}).populate('user_id').populate('product_id');
            console.log(rewardInfo);
            const api = await ApiPromise.create({ provider: wsProvider });

            const keyring = new Keyring({ type: 'sr25519' });

            // Add Alice to our keyring with a hard-derivation path (empty phrase, so uses dev)
            const alice = keyring.addFromUri('//Alice');
            const { nonce } = await api.query.system.account(alice.address);

            console.log('Sending', rewardInfo.amount * rewardInfo.product_id.reward_amount, 'from', alice.address, 'to', rewardInfo.user_id.wallet, 'with nonce', nonce.toString());

            const transfer = api.tx.balances.transfer(String(rewardInfo.user_id.wallet), (rewardInfo.amount * rewardInfo.product_id.reward_amount) * 1e12);

            // Sign and send the transaction using our account
            await transfer.signAndSend(alice, ({ events = [], status, txHash }) => {
                console.log(status.isFinalized ? `😉 Finalized. Block hash: ${status.asFinalized.toString()}` : `Current transaction status: ${status.type}`)

                events.forEach( async ({ _, event: { data, method, section } }) => {
                    if ((section + ":" + method) === 'system:ExtrinsicFailed' ) {
                    } else if (section + ":" + method === 'system:ExtrinsicSuccess' ) {
                        console.log(`❤️️ Transaction successful! tx hash: ${txHash}`)
                    }
                });
            });
            
        }

        res.status(200).json({
            status: 'success',
            data: {
                doc
            }
        });

    } catch (error) {
        next(error);
    }
};

exports.deleteReward = base.deleteOne(Reward);
exports.addReward = base.createOne(Reward);
