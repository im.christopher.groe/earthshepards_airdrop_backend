const Notification = require('../models/notificationModel');
const base = require('./baseController');
const APIFeatures = require('../utils/apiFeatures');
const cronJob = require('../utils/cronJob');

exports.getAllNotifications = base.getAll(Notification);
exports.getNotification = base.getOne(Notification);

// Don't update password on this 
exports.updateNotification = base.updateOne(Notification);
exports.deleteNotification = base.deleteOne(Notification);
exports.addNotification = async(req, res, next) => {
    try {
        const doc = await Notification.create(req.body);

        cronJob.updateMailCronJob(req.body.week * 7);

        res.status(200).json({
            status: 'success',
            data: {
                doc
            }
        });

    } catch (error) {
        next(error);
    }
};
