const { promisify } = require("util");
const jwt = require("jsonwebtoken");
const User = require("../models/userModel");
const AppError = require("../utils/appError");
const base = require('./baseController');
const { KeyObject } = require("crypto");
const createWallet = require('../utils/createWallet');
const sendMail = require("../utils/sendMail");
const fetch = require('node-fetch')

const createToken = (id, email) => {
    return jwt.sign({
            id: id,
            email: email
        },
        process.env.JWT_SECRET, {
            expiresIn: process.env.JWT_EXPIRES_IN,
        },
    );
};


const sendHubspotData = async(data) => {
    console.log('data',data)
    const res = await fetch('https://api.hubapi.com/crm/v3/objects/contacts?hapikey=',{
        method:'POST',
        headers:{
            'Content-Type': 'application/json', 
            'Authorization': 'Bearer pat-na1-a60fed72-4a2b-4840-b03a-d86b34f67f24'        
        },
        body:JSON.stringify({properties:data}),
        redirect: 'follow'
    })

    console.log('res',res)
}

var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{3})+$/;

/**
 * Login user and create token
 *
 * @param  [string] email
 * @param  [string] password
 * @param  [boolean] remember_me
 * @return [string] access_token
 * @return [string] token_type
 * @return [string] expires_at
 */
exports.login = async(req, res, next) => {
    try {
        const { email, password } = req.body;

        // 1) check if email and password exist
        if (!email || !password) {
            return next(
                new AppError(401, "fail", "Please provide email or password"),
                req,
                res,
                next,
            );
        }

        if(!email.match(mailformat)) {
            return next(
                new AppError(401, "fail", "Invalid Email"),
                req,
                res,
                next,
            );
        }

        if(password.length < 8 ) {
            return next(
                new AppError(401, "fail", "Your password must be longer than 8 characters."),
                req,
                res,
                next,
            );
        }
        // 2) check if user exist and password is correct
        const user = await User.findOne({
            email,
        }).select("+password");
        console.log(user);

        if (!user || !(await user.correctPassword(password, user.password))) {
            return next(
                new AppError(401, "fail", "Email or Password is wrong"),
                req,
                res,
                next,
            );
        }

        // if (user.whitelisted == false) {
        //     return next(
        //         new AppError(401, "fail", "Please wait until approve whitelisting"),
        //         req,
        //         res,
        //         next,
        //     );
        // }

        const token = createToken(user.id, user.email);

        // Remove the password from the output
        user.password = undefined;

        res.status(200).json({
            status: "success",
            token,
            data: {
                user,
            },
        });
    } catch (err) {
        next(err);
    }
};

exports.isauthorized = async (req, res, next) => {
    try {
        console.log('auth');
        res.status(200).json({
            status: "success",
            data: req.user
        });
    } catch (err) {
        next(err);
    }
}

exports.signup = async(req, res, next) => {
    try {
        console.log(req.body)
        const {email, password, passwordConfirm} = req.body;

        if (!email || !password || !passwordConfirm) {
            return next(
                new AppError(401, "fail", "Please provide email or password"),
                req,
                res,
                next,
            );
        }

        if(!email.match(mailformat)) {
            return next(
                new AppError(401, "fail", "Invalid Email"),
                req,
                res,
                next,
            );
        }

        const finduser = await User.findOne({ email });
        if(finduser) {
            return next(
                new AppError(401, "fail", "E11000 duplicate key error"),
                req,
                res,
                next,
            );
        }

        if(password.length < 8 ) {
            return next(
                new AppError(401, "fail", "Your password must be longer than 8 characters."),
                req,
                res,
                next,
            );
        }

        if (password != passwordConfirm) {
            return next(
                new AppError(401, "fail", "Your password and confirmation password are not the same."),
                req,
                res,
                next,
            );
        }

        const { publicKey, mnemonic } = await createWallet();
        console.log(publicKey, mnemonic);
        
        const mailData = {
            to: email,
            subject: 'Confirm your subscription.',
            html: `<body style="display: flex; justify-content: center;">
                        <div style="max-width: 550px;">
                            <div style="border: 1px solid #ccc;">
                                <p style="padding: 30px; color: #546a78; font-size: 18px;">
                                    Please click the button below to confirm that you'd like to receive email from Earth Shepards.
                                </p>
                                <div style="display: flex; padding-left: 140px;">
                                    <a href="https://earthshepards.com/confirmed"
                                        style="padding: 15px 20px 15px 20px; background-color: #0d5586; border-radius: 5px; font-size: 20px; font-family: Helvetica,Arial,serif; text-decoration: none; color: white;">
                                        Confirm my subscription
                                    </a>
                                </div>
                                <p style="padding: 30px; color: #546a78; font-size: 18px;">
                                    Thank you!
                                    <br/>
                                    Earth Shepards
                                </p>
                            </div>
                            <div style="border: 1px solid #ccc; margin-top: 20px;">
                                <p style="padding: 30px; color: #546a78; font-size: 14px; text-align: center;">
                                    If you received this in error or do not want to subscribe, simply ignore this email and do not click the button.
                                </p>
                            </div>
                    
                        </div>
                    </body>`
            // `<body><p> Your account successfully added in Earthshepards. Please sign in https://app.earthshepards.com/retro/products with ${email}</p></body>`
        };
        // const mailSent = await sendMail(mailData);
        // console.log(mailSent);

        // if (!mailSent) {
        //     res.status(201).json({
        //         status: "failed",
        //         message: "Incorrect email"
        //     });
        //     return;
        // }



        const user = await User.create({
            wallet: publicKey,
            mnemonic: mnemonic,
            ...req.body
        });



        const token = createToken(user.id, user.email);

        const data = req.body

        sendHubspotData({
            email:data.email,
            firstname:data.name.split(' ')[0],
            lastname:data.name.split(' ')[1]??'',
            company:'Hubspot',
            phone: "(555) 555-5555",
            website: "hubspot.com",
            lifecyclestage: "marketingqualifiedlead"
        })
        
        // user.email_verify_code = undefined;
 
        //user.password = undefined;

        res.status(401).json({
            status: "success",
            token,
            data: {
                user,
            },
        });
    } catch (err) {
        next(err);
    }
};

exports.changePassword = async(req, res, next) => {
    try {
        
        const { _id } = req.body;

        console.log(req.body);

        const user = await User.findOne({
            _id,
        }).select("+password");

        if(!user) {
            return next(
                new AppError(201, "fail", "User not exists"),
                req,
                res,
                next,
            );
        }

        console.log(user);
        if (!(await user.correctPassword(req.body.old_password, user.password))) {
            return next(
                new AppError(201, "fail", "Wrong old Paasword!"),
                req,
                res,
                next,
            );
        }

        console.log("1");

        if (req.body.password != req.body.password_confirmation) {
            return next(
                new AppError(201, "fail", "Wrong Confirm!"),
                req,
                res,
                next,
            );
        }
        console.log("2");

        user.password = req.body.password;
        console.log(user.password);
        user.save();

        res.status(200).json({
            status: "Successfully changed!",
        });
    } catch (err) {
        next(err);
    }
};


exports.logout = async(req, res, next) => {
    res.status(200).json({
        status: "success",
    });
};


exports.protect = async(req, res, next) => {
    try {
        // 1) check if the token is there
        let token;
        if (
            req.headers.authorization &&
            req.headers.authorization.startsWith("Bearer")
        ) {
            token = req.headers.authorization.split(" ")[1];
        }
        if (!token) {
            return next(
                new AppError(
                    401,
                    "fail",
                    "You are not logged in! Please login in to continue",
                ),
                req,
                res,
                next,
            );
        }

        // 2) Verify token
        const decode = await promisify(jwt.verify)(token, process.env.JWT_SECRET);

        // 3) check if the user is exist (not deleted)
        const user = await User.findById(decode.id);
        if (!user) {
            return next(
                new AppError(401, "fail", "This user is no longer exist"),
                req,
                res,
                next,
            );
        }

        req.user = user;
        next();
    } catch (err) {
        next(err);
    }
};

// Authorization check if the user have rights to do this action
exports.restrictTo = (...roles) => {
    return (req, res, next) => {
        if (!roles.includes(req.user.menuroles)) {
            return next(
                new AppError(401, "fail", "You are not allowed to do this action"),
                req,
                res,
                next,
            );
        }
        next();
    };
};