const Product = require('../models/productModel');
const base = require('./baseController');
const APIFeatures = require('../utils/apiFeatures');

exports.getAllProducts = async(req, res, next) => {
    try {

        let doc;

        if(req.body.price_min !== undefined && req.body.price_max !== undefined) {
            console.log(req.body);
            if(req.body.user_id === '') {
                doc = await Product.find({ status: req.body.status }).where('price').gte(req.body.price_min).lte(req.body.price_max).populate('user_id');
            } else {
                doc = await Product.find({ user_id: req.body.user_id, status: req.body.status }).where('price').gte(req.body.price_min).lte(req.body.price_max).populate('user_id');
            }
        } else {
            doc = await Product.find(req.body).populate('user_id');
        }
        
        res.status(200).json({
            status: 'success',
            results: doc.length,
            data: {
                data: doc
            }
        });
        
    } catch (error) {
        next(error);
    }

};
exports.getProduct = base.getOne(Product);

// Don't update password on this 
exports.updateProduct = base.updateOne(Product);
exports.deleteProduct = base.deleteOne(Product);
exports.addProduct = base.createOne(Product);
