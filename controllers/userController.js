const User = require('../models/userModel');
const base = require('./baseController');
const APIFeatures = require('../utils/apiFeatures');

const { ApiPromise, WsProvider } = require('@polkadot/api');
const { Keyring } = require('@polkadot/keyring');
const sendMail = require('../utils/sendMail');

const wsProvider = new WsProvider("wss://www.shearnode.com");

exports.getAllUsers = base.getAll(User);
exports.getUser = base.getOne(User);

// Don't update password on this 
exports.updateUser = base.updateOne(User);
exports.deleteUser = base.deleteOne(User);

exports.getUserByWallet = async(req, res, next) => {
    try {
        const data = await User.findOne(req.body);

        res.status(200).json({
            status: 'success',
            data: {
                data
            }
        });
    } catch (error) {
        next(error);
    }
};

exports.addUser = async(req, res, next) => {
    try {
        console.log(req.body)
        const { publicKey, mnemonic } = await createWallet();
        console.log(publicKey, mnemonic);

        const doc = await User.create(req.body);

        const mailData = {
            to: doc.email,
            subject: 'Welcome to Earthshepards',
            html: `<body><p> Your account successfully added in Earthshepards. Please sign in https://app.earthshepards.com/retro/products with ${doc.email}</p></body>`
        };
        sendMail(mailData);

        res.status(200).json({
            status: 'success',
            data: {
                doc
            }
        });

    } catch (error) {
        next(error);
    }
};

exports.transfer = async(req, res, next) => {
    try {
        const api = await ApiPromise.create({ provider: wsProvider });

        const keyring = new Keyring({ type: 'sr25519' });

        const from = keyring.addFromMnemonic(req.user.mnemonic);
        console.log('aaa', from.address);
        const { nonce } = await api.query.system.account(from.address);
        console.log('aaa2');
        
        const transfer = api.tx.balances.transfer(String(req.body.toAddress), Number(req.body.amount) * 1e12);
        console.log('aaa3');

        await transfer.signAndSend(from, ({ events = [], status, txHash }) => {
            console.log(status.isFinalized ? `😉 Finalized. Block hash: ${status.asFinalized.toString()}` : `Current transaction status: ${status.type}`)

            events.forEach( async ({ _, event: { data, method, section } }) => {
                if ((section + ":" + method) === 'system:ExtrinsicFailed' ) {
                } else if (section + ":" + method === 'system:ExtrinsicSuccess' ) {
                    console.log(`❤️️ Transaction successful! tx hash: ${txHash}`)
                }
            });
        });

        res.status(200).json({
            status: 'success',
        });

    } catch (error) {
        next(error);
    }
};

exports.whitelist = async(req, res, next) => {
    try {
        const doc = await User.findByIdAndUpdate(req.params.id, req.body, {
            new: true,
            runValidators: true
        });

        if (!doc) {
            return next(new AppError(404, 'fail', 'No document found with that id'), req, res, next);
        }

        if(req.body.whitelisted) {
            const mailData = {
                to: doc.email,
                subject: 'Whitelist Approved',
                html: `<body><p> Your account successfully added in whitelist. Please sign in https://app.earthshepards.com/retro/products with ${doc.email}</p></body>`
            };
            sendMail(mailData);
        }

        res.status(200).json({
            status: 'success',
            data: {
                doc
            }
        });
    } catch (error) {
        next(error);
    }
}


exports.sendMails = async(req, res, next) => {
    try {
        for (let i = 0; i < req.body.to.length; i ++) {
            const mailData = {
                to: req.body.to[i],
                subject: 'Message from Earthshepards',
                html: `<body><p>${req.body.message}</p></body>`
            };
            sendMail(mailData);
        }

        res.status(200).json({
            status: 'success',
        });
    } catch (error) {
        next(error);
    }
}