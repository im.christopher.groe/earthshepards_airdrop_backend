const mongoose = require("mongoose");

const productSchema = new mongoose.Schema({
    user_id: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    },
    name: {
        type: String,
        require: true
    },
    image: {
        type: String,
    },
    detail: {
        type: String,
    },
    bill_doc: {
        type: String,
    },
    reward_amount: {
        type: Number,
        default: 0
    },
    price: {
        type: Number,
        default: 0
    },
    description: {
        type: String,
        default: ""
    },
    status: {
        type: String,
        enum: ['Waiting', 'Accepted', 'Declined'],
        default: 'Waiting'
    }
});

const Product = mongoose.model("Product", productSchema);
module.exports = Product;