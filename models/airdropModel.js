const mongoose = require("mongoose");

const airdropSchema = new mongoose.Schema({
    wallet: {
        type: String,
        default: "",
        unique: true
    },
});

const Airdrop = mongoose.model("Airdrop", airdropSchema);
module.exports = Airdrop;