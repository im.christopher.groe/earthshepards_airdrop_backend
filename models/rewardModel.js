const mongoose = require("mongoose");

const rewardSchema = new mongoose.Schema({
    user_id: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    },
    product_id: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Product'
    },
    bill_doc: {
        type: String,
    },
    amount: {
        type: Number,
        default: 0
    },
    description: {
        type: String,
        default: ""
    },
    status: {
        type: String,
        enum: ['Waiting', 'Accepted', 'Declined'],
        default: 'Waiting'
    }
});

const Reward = mongoose.model("Reward", rewardSchema);
module.exports = Reward;