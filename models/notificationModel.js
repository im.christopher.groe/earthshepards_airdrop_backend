const mongoose = require("mongoose");

const notificationSchema = new mongoose.Schema({
    text: {
        type: String
    },
    week: {
        type: Number
    }
});

const Notification = mongoose.model("Notification", notificationSchema);
module.exports = Notification;