const Notification = require('../models/notificationModel');
const User = require('../models/userModel');
const sendMail = require('./sendMail');
const cron = require('node-cron');

const sendNotification = async () => {
    try {
        console.log('aaa');
        const notification = await Notification.find({}).sort({'_id' : -1}).limit(1);
        
        const users = await User.find({});

        for (let user of users) {
            const mailData = {
                to: user.email,
                subject: 'Notification from Earthshepards',
                html: `<body>${notification[0].text}</body>`
            };
            sendMail(mailData);
        }
        
    } catch (error) {
        console.log(error);
    }
};

let mailCron = cron.schedule('* * * * *', function() {
    console.log('sending mails to users');
    sendNotification();
}, {
    scheduled: false
});

const updateMailCronJob = async (week) => {
    try {
        mailCron.stop();
        mailCron = cron.schedule('0 8 */' + week + ' * *', function() {
            console.log('sending mails to users');
            sendNotification();
        });
    } catch (error) {
        console.log(error);
    }

}

module.exports = {updateMailCronJob, sendNotification};