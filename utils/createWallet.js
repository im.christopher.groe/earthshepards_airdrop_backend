const { mnemonicGenerate, cryptoWaitReady } = require('@polkadot/util-crypto');
const { Keyring } = require('@polkadot/keyring');

const createWallet = async () => {
    await cryptoWaitReady();

    const keyring = new Keyring({type: 'sr25519'});
    const mnemonic = mnemonicGenerate();
    const account = keyring.addFromMnemonic(mnemonic);

    console.log(`Address: ${account.address}`);
    console.log(`Mnemonic: "${mnemonic}"`);
    return { publicKey: account.address, mnemonic};
}

module.exports = createWallet;